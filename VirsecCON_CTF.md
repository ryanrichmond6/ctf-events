
[[_TOC_]]

---

## VirSecCON CTF Overview

*Host:* John Hammond
*Participants include over 2200 teams, 1546 teams on the scoreboard*
**Ryan Richmond:** *gingerknight* placed 177th
**Eric Anderson:** *bottaflokka* placed 189th

### CTF Experience

Eric and Ryan were interested in attending the VirSecCON conference that was hosted by Twitch infosec personality *nahamsec* with special guests from the Bug Bouny workforce and Offensive Security professionals from the private sector and Officers in the DOD US Air Force.

Later it was discovered that John Hammond would host a **FREE** CTF for all participants in the conference so Eric and Ryan joined forces and signed up to compete. This was Eric's Second CTF competition and Ryan's first.

### CTF Background Information
CTF competitions require outside of the box thinking, creativity, extreme problem solving skills and a thorough understanding of all things Cyber (networking, steganography, cryptography, binary exploitation, web applications and scripting). CTF events are hosted regularly and the most famous event is hosted at DEFCON security conference were the most elite Cyber Competitors who qualify compete.

The CTF contained the following categories: binary exploitation, web, cryptography, scripting, steganography, and miscellaneous.

## Challenges

---

### Stego

Steganography is still used to transport secret images securely to another individual without alerting a security appliance and to ensure only the target can read the message. The steganography challenges required various techniques to detect the presence of a flag.
1. The first challenge required changing the visual representation of the image through multiple color palettes in order to detect the text.
   - Tool: stegsolve with jar file for switching color palettes.
1. The second required detecting data in slack space of the text file.
   - Tool: stegsnow -QCS
3. The third challenge required finding the binary data which was present in the file, and then translating that data into readable UNICODE characters.
   - stegsnow -Q; hexeditor out.txt; translate hex.
4. The final challenge required multiple steps to discover padded data in the image, then extracting the raw padded data and then translating the data to hex code which can then be converted through a script for manipulation to its final form, ascii text.  
   - stegracker

---

### Binary

Binary challenges famously require 'House' exploits. The C code is often written using gets, puts, and malloc with either predetermined buffer sizes or user determined.
#### Dracula
1. The dracula challenge was an exploit related to array allocation in memory with C code. The ascii art (Count Dracula) would ask for a number to count up to. 1, 2, and 3 would result in the count counting up to the number and successfully exiting (code 0). Any number higher, would result in the count skipping to the final number and exiting (return 0). Negative numbers, special chars, and escape code would result in exit code 1, and quit.
   1. The intial thought was to shove chars into the buffer and force the Power exploit to write past the stack allocation into the .data and .text section of the exploit. Pushing ("A" * 120) chars to the stack resulted in failure and no result. Then the idea of pushing large numbers across the wire came to mind and shoved '111111111111' (twelve 1's). This resulted in a different return value signifying a limited size for a preallocated array in C.
   1. After working through different number sizes (99999999, 888888888, 777777777) the array finally triggered a failure when writing to a specific place in memory to leak the flag code.

---

### Forensics / RE

#### Tragic number
   1. The zip file wouldn't open from the prompt.
   2. Use `file` on the zip file and discover that it is not a zip.
   3. Dump the file contents with Hexdump and Objdump to read the hex values of the file.
   4. Then confirm with binary ninja to see that there is no header information for the file.
![alt tragic1][tragic1]
   5. Edit the header information from `48 34 43 4B` to the zip header information `50 4B 03 04`
![alt tragic2][tragic2]
   6. Results in the file being unzipped successfully and you can read the flag.txt file inside.

#### Forgotten Password
   1. Recover password from hash.
   2. Two files provided, etc/passwd file and etc/shadow file.
```
SHADOW
=== break ===
john:$6$s.e1vJFx9a3RMVUM$etzkgAvXdiyR/5vBWFzC4J.ECadUJkDi6MUiOEJfc1mo3Z7VeWZKv1iWSvW8XQ8zC5bK8kTvWCs7iw5Hy3ve/0:18205:0:99999:7:::
systemd-coredump:!!:18180::::::
=== break ===
```
```
Passwd
=== break ===
gdm:x:123:128:Gnome Display Manager:/var/lib/gdm3:/bin/false
john:x:1000:1000:john,,,:/home/john:/bin/bash
systemd-coredump:x:999:999:systemd Core Dumper:/:/sbin/nologin
lightdm:x:124:130:Light Display Manager:/var/lib/lightdm:/bin/false
_rpc:x:125:65534::/run/rpcbind:/usr/sbin/nologin
=== break ===
```

   3. From the two files use hashcat to find the hash sample and which mode to run it in.
      1. Discovered mode 1800 for SHA1 hash to be used.
   1. Run hashcat against the hash using a rockyou.txt word list.
```
$6$s.e1vJFx9a3RMVUM$etzkgAvXdiyR/5vBWFzC4J.ECadUJkDi6MUiOEJfc1mo3Z7VeWZKv1iWSvW8XQ8zC5bK8kTvWCs7iw5Hy3ve/0:whiterose
```

---

### Crypto


#### classic.txt
   1. file contained the following contents:
```
n = 77627938360345301510724699969247652387657633828943576274039402978346703944383

e = 65537

c = 62899945974090753231979111677615029855602721049941681356856158761811378918268
```
  2. This is RSA key formatting from my studies at UA (NSA program). The N is the result of two prime numbers being multiplied together (AKA Public Key), and the E is the exponent in which N is raised to to create the large secret to which the cipher text is encrypted with. The n and e are very small for current operations. RSA is recommended to use 1048 bit numbers, which are significantly larger, so this can be calculated by hand or through different calculators to get d, the decrypt key needed to decipher the c, cipher text.
  3. By hand would require factoring N to the two prime numbers p, and q.
     1. Using python, take the square root of our number to find about where the first prime, p, will reside.
     2. Find the prime number that exists there for p using modulus (%)
     3. Find q now that we know p by multiplying the numbers together to get n
     4. Once we have n, p, q, calculate phi `phi = (p -1) * (q -1)`
     5. Now that we have phi we can caculate d the private key `(d * e) mod phi = 1 `  

#### hotdog.txt
   1. file contained the following contents:
```
n = 609983533322177402468580314139090006939877955334245068261469677806169434040069069770928535701086364941983428090933795745853896746458472620457491993499511798536747668197186857850887990812746855062415626715645223089415186093589721763366994454776521466115355580659841153428179997121984448771910872629371808169183

e = 387825392787200906676631198961098070912332865442137539919413714790310139653713077586557654409565459752133439009280843965856789151962860193830258244424149230046832475959852771134503754778007132465468717789936602755336332984790622132641288576440161244396963980583318569320681953570111708877198371377792396775817

c = 387550614803874258991642724003284418859467464692188062983793173435868573346772557240137839436544557982321847802344313679589173157662615464542092163712541321351682014606383820947825480748404154232812314611063946877021201178164920650694457922409859337200682155636299936841054496931525597635432090165889554207685
```
  2. The e is close in size to n. This signifies that it is susceptible to a wiener attack. Launch rsa-tools and perform the attack
     1. results in decrypting C successfully.

---

### Programming

#### 4 digit PIN
   1. A page loads after netcat'ing in. That a 4 digit pin is required to enter.
   2. Python and pwntools to help:

```python
from pwn import * #import pwn tools
for i in range (0,9999): # we know the pin has to be four digits and the max is 9
    num = str(i) # convert integer to string
    code = num.zfill(4) # use the zfill to fill up the empty spots like for digit 10, make it 0010
    r = remote("jh2i.com",50031) # connect to remote server
    r.recvuntil("Please enter the 4-digit pincode:") #continue receiving the data from the connection until that string matches
    r.sendline(code) # send over the pin number
    response = r.recvline() #receive the response
    r.close() # close the connection
    if b"INCORRECT" not in response: # if INCORRECT not in response print the below code and break. this will loop back up to the for loop to repeat.
        print(code)
        break
```

#### Grammer
   1. netcat to the server and discover that it checks for the flag value one letter at a time. If it is there it responds with Correct in the Line, if not it states Incorrect adn returns to prompt.
   2. The following script will discover letters one value at a time.

```python
from pwn import *
from string import printable

host = "jh2i.com"
port = "50012"

flag_so_far = list("LLS{")

while 1:
    for p in printable:
        i = len(flag_so_far)
        use = "".join(flag_so_far) + p
        print(use)
        r = remote(host, port)
        r.recvuntil("> ")
        r.sendline(use)

        r.recvuntil("WRONG!")
        print("good? -->" + use)
        #returncode = r.recvline() <couldn't figure out the return line to cycle this automatically, so the script will end each time when it finds the correct letter>
        #if b"WRONG!" not in returncode:
        #    flag_so_far.append(p)
        #    print(flag_so_far)
        #    break
        r.close()
```

#### 2048
   1. This challenge gives the player a text file that is massive in size and is base 64 encoded. It requires multiple decodes to get back to ascii

```python
def decode(msg):
    base64_bytes = msg.encode('ascii')
    message_bytes = base64.b64decode(base64_bytes)
    message = message_bytes.decode('ascii')
    return message
with open("out.txt", "r") as text:
    base64_message = text.read()
    message = decode(base64_message)
    msg = message
    for x in range(50):
        print("")
        print(f"This is iteration {x}")
        print("")
        text = decode(msg)
        print(text)
        msg = text

```

#### Loopback
   1. [Published Solve by bottaflokka](https://ctftime.org/writeup/19510) <-- **Great writeup!!!**

```python
#!/usr/bin/env python3

from scapy.all import *

packets = rdpcap('loopback.pcap')

rawlist = []

for i in packets:
    x = str(i[Raw].load)
    for k in range(5,8):
        if x[-k] == x[-8]:
            rawlist.append(x[-8])

testflag = ""
new_rawlist = rawlist[::6]
for letter in new_rawlist:
    testflag += str(letter)

start = testflag.find('LLS{')
end = testflag.find('}', start) + 1
flag = testflag[start:end]

print("The Flag is: " + flag)
```

---

### Misc (UNIX configuration)
- These challenges were not annotated as detailed as others. We will just reference them in no particular order.
1. One challenge required the user to break out of a rsh (restricted shell) but after searching the definitions and permissions alotted by rsh, you can perform calls to the shell with other commands. Simple, read the manual
2. Second challenge required to execute code when presented only with a manual log in. This can be performed by typing `:! (shell command)` This will create a second command area where the you will escape out of the man page execute the action and upon hitting `RETURN` you will return to the manual.
3. Third was misconfiguration with file permissions. You could not change into the folder needed to get the flag, but the permissions allowed you to list the contents and then pair that into a script to list, then concatonate the folders contents to another file which would allow you to read. Or you can scp the folder out of the ssh session to a local directory for reading.
4. Fourth was to perform a bash script to grep for the contents of a file in a secret location, and then executing the discovered script in a different location by redirecting output to another file which can be executed.

---

### Web

#### Countdown.
   1. A simple web page with PHP script displaying a countdown timer on a tv. When you press the link to defuse the bomb, it explodes.
   2. After loading the page into burp, and sending the intercepted traffic to repeaters, you can see that the result of clicking link is the bomb page loading.
   3. After playing with cookie values (as it showed something like EPOCH time), to a very large value, the page defuse page loads, showing defused.



[tragic1]: ./tragic1.png "zip hex not present"
[tragic2]: ./tragic2.png "zip hex inserted"
